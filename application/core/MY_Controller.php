<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller{

    public function show($conteudo)
    {
        $html = $this->load->view("template/header", null, true);
        $html .= $this->load->view("template/navbar", null, true);

        $html .= $conteudo;

        //$html .= $this->load->view("componente/rodape", null, true);
        $html .= $this->load->view("template/footer", null, true);

        echo $html;
    }

}