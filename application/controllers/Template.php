<?php

class Template extends MY_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('MensagemModel','mensagem');
        $this->load->model('AgendaModel','agenda');
    }


    public function index()
    {     
        $data["nome"] = $this->mensagem->info_carrosel();
        $html  = $this->load->view("component/carousel1.php",$data,true);
        $html .= $this->load->view("component/main_index.php",null,true);        
        
        $this->show($html);
    }

    public function portfolio(){
      
        $html  =  $this->load->view("component/carousel2.php",null,true);
        $html .=  $this->load->view("component/imagens_portfolio.php",null,true);
        $html .=  $this->load->view("component/jumbotron.php",null,true);
        
        $this->show($html);
    }

    public function contato(){
        $this->mensagem->salvar();
        
        $html = $this->load->view("component/formulario.php", null, true);
        $html .= $this->load->view("component/agende.php", null, true);
        $html .= $this->load->view("component/mapa.php", null, true); 

        $this->show($html);
    }



    public function config_sms(){
        //$this->mensagem->salvar();
        $data["mensagem"] = $this->mensagem->tabela();
        
        $html = $this->load->view("mensagem/table_sms", $data, true);

        $this->show($html);
    }

    public function edita($id) {                // carregar dados do formulário
        $this->mensagem->edita($id);
        $this->mensagem->carrega($id);
        $html = $this->load->view('Mensagem/form_sms', null, true);

        $this->show($html);
    }

    public function config_agenda(){
        
        $data["agenda"] = $this->agenda->tabela_agenda();
        $html = $this->load->view("Agenda/table_agenda", $data, true);

        $this->show($html);
    }

    public function edita_agenda($id) {                // carregar dados do formulário
        $this->agenda->edita_agenda($id);
      
        $this->agenda->carrega_agenda($id);
   
        $html = $this->load->view('Agenda/form_agenda', null, true);

        $this->show($html);
    }

    public function remove_agenda($id)
    {
        $rs = $this->agenda->remove_agenda($id);
        if($rs)
        {
            redirect(base_url("Template/config_agenda"));
        }
    }

    public function remove_mensagem($cod_sms)
    {
        $rs = $this->mensagem->remove_mensagem($cod_sms);
        if($rs)
        {
            redirect(base_url("Template/config_sms"));
        }
    }

}