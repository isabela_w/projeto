<?php
    defined('BASEPATH') OR exit ('No direct script access allowed');

    class Mensagem extends MY_Controller{

        function __construct(){
            parent::__construct();
            $this->load->model('MensagemModel','mensagem');
        }
       
        public function contato(){
    
            $html = $this->load->view('mensagem/contato',null,true);
            $this->show($html);
        }

    }


?>