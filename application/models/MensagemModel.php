<?php

    defined('BASEPATH') OR exit ('No direct script access allowed');
    include APPPATH.'libraries/table.php';

    class MensagemModel extends CI_Model{
        function __construct(){
            $this->load->library('util/Validator');
        }

        public function salvar(){
            
             if(sizeof($_POST) == 0) return;
            
            if($this->input->post("agenda") == 1)
            {
                if($this->validator->agenda_validate()){
                    $data['nome'] = $this->input->post('nome_agenda');      
                    $data['email'] = $this->input->post('email_agenda');
                    $data['dia'] = $this->input->post('dia_agenda');
                    $data['servico'] = $this->input->post('servico_agenda');
                    $data['comentario'] = $this->input->post('comentario_agenda');
    
                    $this->load->library('Mensagem', null, 'bb');
                    $this->bb->set_agenda($data);
                }
                else echo validation_errors();
            }
            else
            {
                if($this->validator->mensagem_validate()){
                    $data['nome'] = $this->input->post('nome');      
                    $data['email'] = $this->input->post('email');
                    $data['mensagem'] = $this->input->post('mensagem');
    
                    $this->load->library('Mensagem', null, 'aa');
                    $this->aa->set($data);
                }
                else echo validation_errors();
            }
        }

        public function get_all_mensagem(){
            $this->load->library('Mensagem', null, 'aa');
            return $this->aa->getAll();
        }

        public function tabela(){
            $this->load->library('Mensagem', null, 'dados');
            $labels = array('ID','Nome', 'Email', 'Mensagem', '');
            $data = $this->dados->getAll();

            foreach ($data as $key => $val) {
                $data[$key]['botoes'] = $this->action_buttons($val);
            }
    
            $table = new Table($data, $labels);
            return $table->getHTML();
        }

        public function edita($id){
            if(sizeof($_POST) == 0) return;

            if($this->validator->mensagem_update_validate()){
                $dados = $this->input->post('dados');
                $this->load->library('Mensagem', null, 'dados');           
                if($this->dados->atualiza($dados, $id))
                {
                    redirect(base_url("Template/config_sms"));
                }
            }
        }

        public function carrega($id)
        {
            $this->load->library("mensagem", null, "aaa");
            $_POST["dados"] =  $this->aaa->get_mensagem($id);
        }

        private function action_buttons($row){
            $html = '<a href="'.base_url('Template/edita/'.$row['cod_sms']).'"><i class="fas fa-edit mr-3 blue-text title="Editar"></i>';
            $html .= '<a href="'.base_url('Template/remove_mensagem/'.$row['cod_sms']).'"><i class="fas fa-trash-alt mr-3 red-text title="Deletar"></i>';
           
            return $html;
        }

        public function info_carrosel()
        {
            return '<div class="mask rgba-black-light d-flex justify-content-center align-items-center">

                    <!-- Content -->
                    <div class="text-center white-text mx-5 wow fadeIn">
                    <h1 class="mb-4">
                        <strong>Studio VW Sobrancelhas</strong>
                    </h1>
            
                    <p>
                        <strong>Especializado em sobrancelhas e extensão de cílios</strong>
                    </p>
            
                    <p class="mb-4 d-none d-md-block">
                        <strong>Venha conhecer mais sobre nosso trabalho, profissionais bem preparados e de confiança!</strong>
                    </p>
            
                    <a target="_blank" href="https://www.instagram.com/studiovw_sobrancelhas/" class="btn btn-outline-white btn-lg">Clique e conheça
                    </a>
                    </div>
                    <!-- Content -->
            
                </div>';
        }

       public function remove_mensagem($id)
        {
            $this->load->library("mensagem", null, "mensagens");
            return $this->mensagens->remove_mensagem($id);
        }
    
    }
?>