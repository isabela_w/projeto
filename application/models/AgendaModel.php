<?php

    defined('BASEPATH') OR exit ('No direct script access allowed');
    include APPPATH.'libraries/Table_Agenda.php';

    class AgendaModel extends CI_Model{
        function __construct(){
            $this->load->library('util/Validator');
        }

        public function salvar(){
            
             if(sizeof($_POST) == 0) return;
             if($this->validator->agenda_validate()){
             $data['nome'] = $this->input->post('nome_');      
             $data['email'] = $this->input->post('email');
             $data['dia'] = $this->input->post('dia');
             $data['servico'] = $this->input->post('servico');
             $data['comentario'] = $this->input->post('comentario');

             $this->load->library('Agenda', null, 'aa');
             $this->aa->set($data);
             }
 
             else echo validation_errors();
        }

        public function tabela_agenda(){
            $this->load->library('Agenda', null, 'aaa');
            $labels = array('Nome', 'Email','Dia','Serviço', 'Comentario', '');
            $data = $this->aaa->getAll();

            foreach ($data as $key => $val) {
                $data[$key]['botoes'] = $this->action_buttons($val);
            }
    
            $table = new Table($data, $labels);
            return $table->getHTML();
        }

        public function edita_agenda($id){
            if(sizeof($_POST) == 0) return;
            
            if($this->validator->agenda_update_validate()){
                $agenda = $this->input->post('agenda');
                $this->load->library('Agenda', null, 'agendas');           
                if($this->agendas->atualiza($agenda, $id))
                {
                    redirect(base_url("Template/config_agenda"));
                }
            }
        }

        public function carrega_agenda($id)
        {
            $this->load->library("agenda", null, "agendas");
            $_POST["agenda"] =  $this->agendas->get_agenda($id);
        }

        private function action_buttons($row){
            $html = '<a href="'.base_url('Template/edita_agenda/'.$row['id']).'"><i class="fas fa-edit mr-3 blue-text title="Editar"></i>';
            $html .= '<a href="'.base_url('Template/remove_agenda/'.$row['id']).'"><i class="fas fa-trash-alt mr-3 red-text title="Deletar"></i>';
            $html .= '<a href="'.base_url('curriculo/curriculo_'.$row['id']).'.pdf" target="_blank"><i class="far fa-file-pdf red-text title="Visualizar Currículo"></i>';
            return $html;
        }

        public function remove_agenda($id)
        {
            $this->load->library("agenda", null, "agendas");
            return $this->agendas->remove_agenda($id);
        }

        

    }
?>