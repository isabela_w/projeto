<?php
     class Agenda extends CI_Object {

        public function set(Array $data){
            $this->db->insert('agendamentos', $data);
        }

        public function getAll(){
            $sql = "SELECT id, nome, email, dia,servico, comentario FROM agendamentos";

            $rs = $this->db->query($sql);
            return $rs->result_array();
        }

        public function atualiza($data,$id){
            $cond["id"] = $id;
            return $this->db->update("agendamentos", $data, $cond);
        }

        public function get_agenda($id)
        {
            $cond["id"] = $id;
            $rs = $this->db->get_where("agendamentos", $cond);
            return $rs->row_array();
        }

        public function remove_agenda($id)
        {
            $cond["id"] = $id;
            return $this->db->delete("agendamentos", $cond);
        }
        
     }

?>