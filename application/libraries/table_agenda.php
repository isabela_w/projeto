<?php

class Table_Agenda {
    private $data;
    private $labels;
    
    public function __construct($data, $labels){
        $this->labels = $labels;
        $this->data = $data; 
    }

    public function getHTML(){
        $html = $this->header();
        $html .= $this->body();
        return $html. '</table>';
    }

    private function header(){
        $html = '<table class="table table-striped table-bordered">';
        $html .= '<tr>';
        foreach ($this->labels as $rotulo) {
            $html .= "<th>$rotulo</th>";            
        }
        $html .= '</tr>';
        return $html;
    }

    private function body(){
        $html = '';
        foreach ($this->data as $row) {
            $html .= '<tr>';
            foreach ($row as $key => $value) {
                if($key == 'id') continue;
                $html .= "<td>$value</td>";
            }
            $html .= '</tr>';
        }
        return $html;
    }

}