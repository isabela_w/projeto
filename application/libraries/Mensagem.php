<?php
     class Mensagem extends CI_Object {

        public function set(Array $data){
            $this->db->insert('mensagens', $data);
        }

        public function set_agenda(Array $data){
            $this->db->insert("agendamentos", $data);
        }

        public function getAll(){
            $sql = "SELECT cod_sms, nome, email, mensagem FROM mensagens";

            $rs = $this->db->query($sql);
            return $rs->result_array();
        }

        public function atualiza($data,$id){
            $cond["cod_sms"] = $id;
            return $this->db->update("mensagens", $data, $cond);
        }

        public function get_mensagem($id)
        {
            $cond["cod_sms"] = $id;
            $rs = $this->db->get_where("mensagens", $cond);
            return $rs->row_array();
        }

        public function remove_mensagem($id)
        {
           $cond["cod_sms"] = $id;
           return $this->db->delete("mensagens", $cond);
        }

     }

?>