<?php
defined('BASEPATH') OR exit('No direct script acess allowed');
//include APPPATH.'libraries/util/CI_Object.php';
class Validator extends CI_Object{

    public function mensagem_validate(){
        $this->form_validation->set_rules("nome","nome pessoa","trim|required|min_length[4]|max_length[150]");
        $this->form_validation->set_rules("email","email","trim|required|valid_email");
        $this->form_validation->set_rules("mensagem","mensagem","trim|required|min_length[4]|max_length[500]");

        return $this->form_validation->run();
    }

    public function mensagem_update_validate(){
        $this->form_validation->set_rules("dados[nome]","nome pessoa","trim|required|min_length[4]|max_length[150]");
        $this->form_validation->set_rules("dados[email]","email","trim|required|valid_email");
        $this->form_validation->set_rules("dados[mensagem]","mensagem","trim|required|min_length[4]|max_length[500]");

        return $this->form_validation->run();
    }

    public function agenda_validate(){
        $this->form_validation->set_rules("nome_agenda","nome pessoa","trim|required|min_length[4]|max_length[150]");
        $this->form_validation->set_rules("email_agenda","email","trim|required|valid_email");
        $this->form_validation->set_rules("dia_agenda","dia","trim|required");
        $this->form_validation->set_rules("servico_agenda","servico","trim|required");
        $this->form_validation->set_rules("comentario_agenda","comentario","trim|required|min_length[4]|max_length[500]");

        return $this->form_validation->run();
    }

    public function agenda_update_validate(){
        $this->form_validation->set_rules("agenda[nome]","nome pessoa","trim|required|min_length[4]|max_length[150]");
        $this->form_validation->set_rules("agenda[email]","email","trim|required|valid_email");
        $this->form_validation->set_rules("agenda[dia]","dia","trim|required");
        $this->form_validation->set_rules("agenda[servico]","servico","trim");
        $this->form_validation->set_rules("agenda[comentario]","comentario","trim|required|min_length[4]|max_length[500]");

        return $this->form_validation->run();
    }

}