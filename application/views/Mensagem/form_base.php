<div class="container pt-5"><br>
<?= validation_errors('<div class="alert alert-danger">', '</div>') ?>
</div>
<form class="border border-light p-5 mt-5" method="POST" enctype="multipart/form-data"><br> 
    <?= $forms  ?>
    <div class="row">
        <button class="btn btn-info my-4 btn block col-md-6 mx-auto" type="submit">Salvar</button>
    </div>
</form> 