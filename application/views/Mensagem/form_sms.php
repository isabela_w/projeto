<div class="container mt-5 pt-5">
    <div class="row">
        <div class="col-md-6 mx-auto">
            <form class="text-center" action="#" method="post">

            <p class="h4 mb-4">Correio Eletrônico</p>
            <input value="<?= set_value('dados[nome]') ?>" name="dados[nome]" type="text" class="form-control mb-4" placeholder="Nome">
            <input value="<?= set_value('dados[email]') ?>" name="dados[email]" type="text" class="form-control mb-4" placeholder="Email">
            <input value="<?= set_value('dados[mensagem]') ?>" name="dados[mensagem]" type="text" class="form-control mb-4" placeholder="mensagem">

            <button class="btn stylish-color-dark white-text btn-rounded z-depth-0 my-4 waves-effect" type="submit">Enviar</button>
            <!--<input type="file" name="userfile" placeholder="Anexar currículo">-->
            </form>
        </div>
    </div>
</div>