<div class="container">
  <main>
  <!-- Material form register -->
<div class="card mt-5 pt-4">

<h5 class="card-header stylish-color-dark white-text text-center py-4">
    <strong>Registre-se</strong>
</h5>

<!--Card content-->
<div class="card-body px-lg-5 pt-0">

    <!-- Form -->
    <form class="text-center" style="color: #757575;" action="#!">

        <div class="form-row">
            <div class="col">
                <!-- First name -->
                <div class="md-form">          
                    <input type="text" id="nome" class="form-control">
                    <label for="nome">Nome</label>
                </div>
            </div>  
        </div>

        <!-- E-mail -->
        <div class="md-form mt-0">
            <input type="text" id="email" class="form-control">
            <label for="email">E-mail</label>
        </div>

        <!-- Password -->
        <div class="md-form">
            <input type="password" id="senha" class="form-control">
            <label for="senha">Senha</label>
        </div>

        <!-- Sign up button -->
        <button class="btn stylish-color-dark white-text btn-block my-4 waves-effect z-depth-0" type="submit">Registre-se </button>


        <hr>

    </form>
    <!-- Form -->

</div>

</div>
<!-- Material form register -->


  </main>
</div>