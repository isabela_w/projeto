<div id="carousel-example-1z" class="carousel slide carousel-fade">

<!--Indicators-->
<ol class="carousel-indicators">
  <li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>
  <li data-target="#carousel-example-1z" data-slide-to="1"></li>
  <li data-target="#carousel-example-1z" data-slide-to="2"></li>
</ol>
<!--/.Indicators-->

<!--Slides-->
<div class="carousel-inner" role="listbox">

  <!--First slide-->
  <div class="carousel-item active">
    <div class="view">

      <!--Video -->
      <video class="video-intro" autoplay loop muted>
        <source src="https://mdbootstrap.com/img/video/animation-intro.mp4" type="video/mp4" />
      </video>

      <!-- Mask & flexbox options-->
      <?= $nome ?>
     

    </div>
  </div>
  <!--/First slide-->

  <!--Second slide-->
  <div class="carousel-item">
    <div class="view" style="background-image: url('https://i.pinimg.com/originals/82/97/12/829712c35642a6e5f0e2d59d7f70b1d1.gif'); background-repeat: no-repeat; background-size: cover;">

      <!-- Mask & flexbox options-->
      <?= $nome ?>
      <!-- Mask & flexbox options-->

    </div>
  </div>
  <!--/Second slide-->

  <!--Third slide-->
  <div class="carousel-item">
    <div class="view">

      <!--Video source-->

      <div class="view" style="background-image: url('https://img.stpu.com.br/?img=https://s3.amazonaws.com/pu-mgr/default/a0R0f000010b4a1EAA/5c115c65e4b07c4e757b9e75.jpg&w=710&h=462'); background-repeat: no-repeat; background-size: cover;">    

      <!-- Mask & flexbox options-->
      <?= $nome ?>
      <!-- Mask & flexbox options-->

    </div>
  </div>
  <!--/Third slide-->

</div>
<!--/.Slides-->

<!--Controls-->
<a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
  <span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
  <span class="carousel-control-next-icon" aria-hidden="true"></span>
  <span class="sr-only">Next</span>
</a>
<!--/.Controls-->

</div>