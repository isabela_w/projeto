<div class="col-lg-7">

        <!--Google map-->
        <div id="map-container-google-11" class="z-depth-1-half map-container-6" style="height: 400px">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3660.3565394609236!2d-46.55947318502439!3d-23.447600384739513!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cef5ab07cc7161%3A0x86c861d6e3b5499c!2sAv.%20S%C3%A3o%20Lu%C3%ADz%2C%20107%20-%20Vila%20Rosalia%2C%20Guarulhos%20-%20SP!5e0!3m2!1spt-BR!2sbr!4v1569800603444!5m2!1spt-BR!2sbr" 
        width="640" height="400" frameborder="0" style="border:0;" allowfullscreen="">
        </iframe>
        </div>

        <br>
        <!--Buttons-->
        <div class="row text-center">
        <div class="col-md-4">
            <a class="btn-floating lighten-1"><i class="fas fa-map-marker-alt"></i></a>
            <p>Av São Luís, 107</p>
            <p>Guarulhos, SP</p>
        </div>

        <div class="col-md-4">
            <a class="btn-floating lighten-1"><i class="fas fa-phone"></i></a>
            <p>+55 (11) 94587-7487 (WhatsApp)</p>
            <p>Seg - Sáb, 8:00-19:00</p>
        </div>


        <div class="col-md-4">
            <a class="btn-floating  lighten-1"><i class="fas fa-envelope"></i></a>
            <p>studiovw@yahoo.com</p>
            <p>studioparcerias@hotmail.com</p>
        </div>
        </div>

    </div>
  <!--Grid column-->

    </div>
</div>
</section>
<!--Section: Contact v.1-->
</main>