<main>
    
    <div class="container">
    <div class="card mt-4 wow fadeIn">
        
    <h5 class="card-header stylish-color-dark white-text text-center mt-5 pt-4">
        <strong>Deixe uma mensagem</strong>
    </h5>

    <!--Card content-->
    <div class="card-body px-lg-5 pt-0 ">

        <!-- Form -->
        <form class="text-center " style="color: #3E4551;" action="#" method="post">

            <!-- Name -->
                <div class="md-form mt-3">
                    <input type="text" id="nome" name="nome" class="form-control">
                    <label for="nome">Nome</label>
                </div>

                <!-- E-mail -->
                <div class="md-form">
                    <input type="text" id="email" name="email" class="form-control">
                    <label for="email">E-mail</label>
                </div>

                <!--Menssagem-->
                <div class="md-form">
                    <textarea id="msg" name="mensagem" class="form-control md-textarea" rows="3"></textarea>
                    <label for="msg">Mensagem</label>
                </div>

                    <!-- Send button -->
                    <button class="btn stylish-color-dark white-text btn-rounded z-depth-0 my-4 waves-effect" type="submit">Enviar</button>
                    <a href="<?= base_url("Template/config_sms") ?>" class="btn stylish-color-dark white-text btn-rounded z-depth-0 my-4 waves-effect">Editar</a>
                </form>
                <!-- Form -->

            </div>
         </div>
    </div>