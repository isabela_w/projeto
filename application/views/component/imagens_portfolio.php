<section class="text-center">

<!-- Grid row -->
<div class="row">

    <!-- Grid column -->
    <div class="col-lg-4 col-md-12 mb-3">

        <div class="view overlay z-depth-1-half">
            <img src="<?= base_url("assets/img/3.jpeg") ?>" class="img-fluid" alt="">
            <a>
                <div class="mask rgba-white-light"></div>
            </a>
        </div>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-4 col-md-6 mb-3">

        <div class="view overlay z-depth-1-half">
            <img src="<?= base_url("assets/img/2.jpeg")?>" class="img-fluid" alt="">
            <a>
                <div class="mask rgba-white-light"></div>
            </a>
        </div>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-4 col-md-6 mb-3">

        <div class="view overlay z-depth-1-half">
            <img src="<?= base_url("assets/img/4.jpeg")?>" class="img-fluid" alt="">
            <a>
                <div class="mask rgba-white-light"></div>
            </a>
        </div>

    </div>
    <!-- Grid column -->

</div>
<!-- Grid row -->

<!-- Grid row -->
<div class="row">

    <!-- Grid column -->
    <div class="col-md-6 mb-3">

        <div class="view overlay z-depth-1-half">
            <img src="<?= base_url("assets/img/cilios1.jpg") ?>" class="img-fluid" alt="">
            <a>
                <div class="mask rgba-white-light"></div>
            </a>
        </div>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-md-6 mb-3">

        <div class="view overlay z-depth-1-half">
            <img src="<?= base_url("assets/img/cilios2.jpg") ?>" class="img-fluid" alt="">
            <a>
                <div class="mask rgba-white-light"></div>
            </a>
        </div>

    </div>
    <!-- Grid column -->

</div>
<!-- Grid row -->

<!-- Grid row -->
<div class="row">

    <!-- Grid column -->
    <div class="col-md-12 mb-2">

        <div class="view overlay z-depth-1-half">
            <img src="<?= base_url("assets/img/cilios3.jpg") ?>" class="img-fluid" alt="">
            <a>
                <div class="mask rgba-white-light"></div>
            </a>
        </div>

    </div>
    <!-- Grid column -->

</div>
<!-- Grid row -->


</section>