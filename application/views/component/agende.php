<div class="container">
    <!--Section: Contact v.1-->
    
<section class="section pb-5">

<!--Section heading-->
<h2 class="section-heading h1 pt-4">Agende um horário</h2>
<!--Section description-->
<p class="section-description pb-4">Entraremos em contato para confirmar o agendamento</p>

<div class="row">

  <!--Grid column-->
  <div class="col-lg-5 mb-4">

    <!--Form with header-->
    <div class="card">

      <div class="card-body">
        <!--Header-->
        <div class="form-header deep-purple lighten-1">
          <h3><i class="fas fa-envelope"></i> Marque o horário aqui:</h3>
        </div>

        <p>Diga qual melhor dia para o atendimento que seu horário será marcado.</p>
        <br>

        <!--Body-->
        <form class="md-form" action="#" method="post">
        <div class="md-form">
          <i class="fas fa-user prefix grey-text"></i>
          <input type="text" id="nome" name="nome_agenda" class="form-control">
          <label for="nome">Nome</label>
        </div>

        <div class="md-form">
          <i class="fas fa-envelope prefix grey-text"></i>
          <input type="text" id="email" name="email_agenda" class="form-control">
          <label for="email">E-mail</label>
        </div>
     
        <div class="md-form">
          <i class="fas fa-tag prefix grey-text"></i>
          <input type="date" id="dia" name="dia_agenda" class="form-control">
          <label for="dia">Dia</label>
        </div>

        <select name="servico_agenda" class="browser-default custom-select mb-4">
                <option value="0">Tipo de serviço</option>
                <option value="Design de sobrancelhas" >Design de sobrancelhas</option>
                <option value="Design+Henna" >Design+Henna</option>
                <option value="Extensão de cílios" >Extensão de cílios</option>
        </select>

        <div class="md-form">
          <i class="fas fa-pencil-alt prefix grey-text"></i>
          <textarea id="comentario" name="comentario_agenda" class="form-control md-textarea" rows="3"></textarea>
          <label for="comentario">Deixe um comentário.Ou a sua preferência de horário </label>
        </div>
        <input type="hidden" value="1" name="agenda" />
        <div class="text-center mt-4">
          <button class="btn deep-purple lighten-1 white-text">Enviar</button>
          <a href="<?= base_url("Template/config_agenda") ?>" class="btn deep-purple lighten-1 white-text">Editar</a>
        </div>

      </div>
    </form>
  </div>
    <!--Form with header-->

  </div>