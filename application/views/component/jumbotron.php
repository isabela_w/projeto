<section class="card wow fadeIn" style="background-image: url(https://cdn.shopify.com/s/files/1/1699/5797/products/HJ07290_530x@2x.jpg?v=1529392809);">
         
         <!-- Content -->
        <div class="card-body text-dark text-center py-5 px-5 my-5">

            <h1 class="mb-4">
                <strong>Visite nossas redes sociais</strong>
            </h1>
            <p>
                <strong>Fique por dentro das novidades</strong>
            </p>
            <p class="mb-4">
                <strong>E veja mais do nosso portfolio</strong>
            </p>
            <a target="_blank" href="https://www.instagram.com/studiovw_sobrancelhas/" class="btn btn-outline-dark btn-lg">Contato
              
            </a>

        </div>
        <!-- Content -->
    </section>