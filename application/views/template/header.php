<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Studio VW</title>
  
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <link href="<?= base_url("assets/css/bootstrap.min.css") ?>" rel="stylesheet">
  <link href="<?= base_url("assets/css/mdb.min.css") ?> " rel="stylesheet">
  <link href="<?= base_url("assets/css/style.min.css") ?>" rel="stylesheet">
  <style type="text/css">
    /*carrossel*/
    html,
    body,
    header,
    .view {
      height: 100%;
    }

    /* Carousel*/
    .carousel,
    .carousel-item,
    .carousel-item.active {
      height: 100%;
    }
    .carousel-inner {
      height: 100%;
    }

  }

  </style>
</head>

<body>