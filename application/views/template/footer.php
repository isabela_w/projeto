<footer class="page-footer text-center font-small mt-4 wow fadeIn deep-purple lighten-1">

<hr class="my-4">

<!-- Social icons -->
<div class="pb-4">
  <a href="https://www.facebook.com/#" target="_blank">
    <i class="fab fa-facebook-f mr-3"></i>
  </a>

  <a href="https://www.instagram.com/studiovw_sobrancelhas/" target="_blank">
  <i class="fab fa-instagram"></i>
  </a>
</div>
<!-- Social icons -->

<!--Copyright-->
<div class="footer-copyright py-3">
  © 2019 Isabela Wurthmann GU3003621:
  <a href="http://portal.ifspguarulhos.edu.br/" target="_blank"> IFSP GRU </a>
</div>
<!--/.Copyright-->

</footer>
<!--/.Footer-->

<!-- SCRIPTS -->
<!-- JQuery -->
<script type="text/javascript" src="<?= base_url("assets/js/jquery-3.4.1.min.js") ?>"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="<?= base_url("assets/js/popper.min.js") ?>"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="<?= base_url("assets/js/bootstrap.min.js") ?>"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="<?= base_url("assets/js/mdb.min.js") ?>"></script>
<!-- Initializations -->
<script type="text/javascript">
// Animations initialization
new WOW().init();
</script>
</body>

</html>
