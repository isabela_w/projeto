<nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar deep-purple lighten-1">
    <div class="container">

      <a class="navbar-brand" href="<?= base_url("Template")?>">
        <strong>Home</strong>
      </a>
  
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

    
      <div class="collapse navbar-collapse" id="navbarSupportedContent">

        
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="<?= base_url("Template/portfolio") ?>" >Portfólio
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url("Template/contato") ?>" >Contato</a>
          </li>
        </ul>

        <!-- Direita -->
        <ul class="navbar-nav nav-flex-icons">
          <li class="nav-item">
            <a href="https://www.facebook.com/" class="nav-link" target="_blank">
              <i class="fab fa-facebook-f"></i>
            </a>
          </li>
          <li class="nav-item">
            <a href="https://www.instagram.com/studiovw_sobrancelhas/" class="nav-link" target="_blank">
            <i class="fab fa-instagram"></i>
            </a>
          </li>
        </ul>

      </div>

    </div>
  </nav>