<div class="container mb-5">
    <div class="row">
        <div class="col-md-6 mx-auto">

        <div class="container pt-5"><br>
        <?= validation_errors('<div class="alert alert-danger">', '</div>') ?>
        </div>

            <form class="text-center" action="#" method="post">

            <p class="h4 mb-4">Agenda</p>
            <input value="<?= set_value('agenda[nome]') ?>" name="agenda[nome]" type="text" class="form-control mb-4" placeholder="Nome">
            <input value="<?= set_value('agenda[email]') ?>" name="agenda[email]" type="text" class="form-control mb-4" placeholder="Email">
            <input value="<?= set_value('agenda[dia]') ?>" name="agenda[dia]" type="date" class="form-control mb-4" placeholder="Dia">
            <select name="agenda[servico]" class="browser-default custom-select mb-4" placeholder="Servico">
                <option value="0" disabled selected>Tipo de serviço</option>
                <option value="Design de sobrancelhas" <?= set_value('agenda[servico]') == "Design de sobrancelhas" ? 'selected' : '' ?> >Design de sobrancelhas</option>
                <option value="Design+Henna" <?= set_value('agenda[servico]') == "Design+Henna" ? 'selected' : '' ?> >Design+Henna</option>
                <option value="Extensão de cílios" <?= set_value('agenda[servico]') == "Extensão de cílios" ? 'selected' : '' ?> >Extensão de cílios</option>
            </select>
            <input value="<?= set_value('agenda[comentario]') ?>" name="agenda[comentario]" type="text" class="form-control mb-4" placeholder="Comentario">

            <button class="btn stylish-color-dark white-text btn-rounded z-depth-0 my-4 waves-effect" type="submit">Enviar</button>
            <!--<input type="file" name="userfile" placeholder="Anexar currículo">-->
            </form>
        </div>
    </div>
</div>